/*
 * Roger Barker
 * CS4280 p3
 * mapper.cpp -- holds mapping function to get the correct column index for the table
 */

#include "mapper.h"



int mapper(char testChar){
	int column;
	
	if(isalpha(testChar))
		column = 1;
	else if(isdigit(testChar))
		column = 2;
	else if(testChar == '=')
		column = 3;
	else if(testChar == '>')
		column = 4;
	else if(testChar == '<')
		column = 5;
	else if(testChar == ':')
		column = 6;
	else if(testChar == '!')
		column = 7;
	else if(testChar == '+')
		column = 8;
	else if(testChar == '-')
		column = 9;
	else if(testChar == '*')
		column = 10;
	else if(testChar == '/')
		column = 11;
	else if(testChar == '%')
		column = 12;
	else if(testChar == '.')
		column = 13;
	else if(testChar == '(')
		column = 14;
	else if(testChar == ')')
		column = 15;
	else if(testChar == ',')
		column = 16;
	else if(testChar == '{')
		column = 17;
	else if(testChar == '}')
		column = 18;
	else if(testChar == ';')
		column = 19;
	else if(testChar == '[')
		column = 20;
	else if(testChar == ']')
		column = 21;
	else if(isspace(testChar))
		column = 22;
	else
		column = 23;
	
	return column;
}
