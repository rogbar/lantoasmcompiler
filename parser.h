/*
 * Roger Barker
 * CS 4280 - P3
 * parser.h holds declarations for the parser functions
 */

#ifndef PARSER_H
#define PARSER_H
#include "misc.h"
#include "token.h"
#include "scanner.h"
#include <string>
using namespace std;

tokenNode * parser(TokenType ** tt, FILE * fp);

tokenNode * programNT( TokenType ** tk, FILE * fp);
tokenNode * blockNT  ( TokenType ** tk, FILE * fp);
tokenNode * varNT    ( TokenType ** tk, FILE * fp);
tokenNode * typeNT   ( TokenType ** tk, FILE * fp);
tokenNode * mvarsNT  ( TokenType ** tk, FILE * fp);
tokenNode * exprNT   ( TokenType ** tk, FILE * fp);
tokenNode * TNT      ( TokenType ** tk, FILE * fp);
tokenNode * FNT      ( TokenType ** tk, FILE * fp);
tokenNode * RNT      ( TokenType ** tk, FILE * fp);
tokenNode * statsNT  ( TokenType ** tk, FILE * fp);
tokenNode * mStatNT  ( TokenType ** tk, FILE * fp);
tokenNode * statNT   ( TokenType ** tk, FILE * fp);
tokenNode * inNT     ( TokenType ** tk, FILE * fp);
tokenNode * outNT    ( TokenType ** tk, FILE * fp);
tokenNode * ifNT     ( TokenType ** tk, FILE * fp);
tokenNode * loopNT   ( TokenType ** tk, FILE * fp);
tokenNode * assignNT ( TokenType ** tk, FILE * fp);
tokenNode * RONT     ( TokenType ** tk, FILE * fp);

tokenNode * getNode  ( string typeOfNT );
void assignParentAndDepth( tokenNode ** par, int num);

#endif
