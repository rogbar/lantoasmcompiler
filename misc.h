/*
 * Roger Barker
 * CS4280 p3
 * misc.h -- holds declarations for misc functions
 */
 
#ifndef MISC_H
#define MISC_H

#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <vector>
#include "token.h"
#include "scanner.h"
using namespace std;

int verifyFileInput(int argc, char** argv);
int checkExtension(string targetFileName, string ext, bool input);

string userInput();
string spaces(int level);

TokenType *postProcessor(TokenType *tt);

void printAppropro(tokenID ty);
void error(string msg, int line);

#endif
