/*
	Roger Barker
	CS4280 P3
	staticSemantics.cpp does traversals for the static semantics and code generation
	of the compiler project for the semester
*/
#include "staticSemantics.h"

#define SIZE 100
//necessary globals
STV * stv = new STV;
static int varCounter = 0;
static int countPush = 0;
static int labelCount = 0;
static int dist = -1;
static vector<string> vars;

//inorder prints root child1 child2 child3 child4
void traverse(tokenNode * root, int depth){
	
	//root is NULL return
	if(root == NULL){
		return;
	}
	
	if(root->nonTerminalVal == "<var>" || root->nonTerminalVal == "<mvars>"){
		insertToSTV(root,depth);
	}else if(root->token1 != NULL && root->token1->tkType == IDTk){
		bool test = verifyNode(root,false);
		if(test){
			cout << "defined\n";
		}else{
			cout << "not defined\n";
			string err = "Error::Use of undefined variable at line: ";
			error(err,root->token1->line);
		}
	}

    //print children
	traverse(root->child1, depth + 1);
	traverse(root->child2, depth + 1);
	traverse(root->child3, depth + 1);
    traverse(root->child4, depth + 1);
}


//call static semantics if you need to check a rule the BNF can't check
void insertToSTV(tokenNode * root, int depth){
	tokenID check = root->token1->tkType;
	if(stv->stv.size() == 0){
		stv->stv.push_back(root);
		cout << "pushed\n";
	}else if(verifyNode(root,true)){
		stv->stv.push_back(root);
		cout << "pushed\n";
	}else{
		cout << "exists\n";
	}
}

//call to process IDs and to check if variable exists on the stack
bool verifyNode(tokenNode * root,bool defineIt){
	bool ret = true;
	bool found = false;
	for(int x = 0; x < stv->stv.size(); x++){
		if(root->token1->value == stv->stv[x]->token1->value){
			found = true;
		} 
	}
	
	//if we are defining and it's not found; define it!
	if(defineIt && !found){
		ret = true;
	}
	//if we are defining and it's found; don't define it!
	else if(defineIt && found){
		ret = false;
	}
	//if we are not defining and it's not found; notify failure!
	else if(!defineIt && !found){
		ret = false;
	}
	//if we are not defining and it's not found; notify success!
	else if(!defineIt && found){
		ret = true;
	}
	return ret;
}

//function to traverse the tree and do code generation
void generateThatTraversal(tokenNode * root, ofstream &out){
	//root is NULL return
	if(root == NULL){
		return;
	}
	
	if(root->nonTerminalVal == "<program>"){
		//generate code for children
		generateThatTraversal(root->child1,out);
		generateThatTraversal(root->child2,out);
		generateThatTraversal(root->child3,out);
		generateThatTraversal(root->child4,out);
		
		//pop global variables
		while(countPush > 0){
			out << "\tPOP\n";
			countPush --;
		}
		
		//Stop processing
		out << "\tSTOP\n";
		
		//reset variables used to 0
		for(int i = 0; i < varCounter; i++){
			out << "\tV" << i << " 0\n";
		}
		for(int k = 0; k < vars.size(); k++){
			out << "\t" << vars[k] << " 0\n";
		}
	}else if(root->nonTerminalVal == "<block>"){
		//generate code for children of block
		generateThatTraversal(root->child1,out);
		generateThatTraversal(root->child2,out);
		generateThatTraversal(root->child3,out);
		generateThatTraversal(root->child4,out);
	}else if(root->nonTerminalVal == "<var>" || root->nonTerminalVal == "<mvars>"){
		//generate code for var and mvars
		if(root->token1->tkType == IDTk){
			countPush ++;
			stv->stv.push_back(root);
			out << "\tPUSH\n";
			out << "\tLOAD\t" << root->token1->value << endl;
			out << "\tSTACKW\t0\n";
			vars.push_back(root->token1->value);
		}
		
		//generate code for children of var and mvars
		generateThatTraversal(root->child1,out);
		generateThatTraversal(root->child2,out);
		generateThatTraversal(root->child3,out);
		generateThatTraversal(root->child4,out);
	}else if(root->nonTerminalVal == "<type>"){
		//generate code for children of type
		generateThatTraversal(root->child1,out);
		generateThatTraversal(root->child2,out);
		generateThatTraversal(root->child3,out);
		generateThatTraversal(root->child4,out);
	}else if(root->nonTerminalVal == "<expr>"){
		//generate code for expression and its children
		string argR;
		if(root->token1 != NULL){
			if(root->token1->tkType == MULTTk){
				argR = makeNameOrLabel("name");
				generateThatTraversal(root->child2,out);
				out << "\tSTORE\t" << argR << endl;
				generateThatTraversal(root->child1,out);
				out << "\tMULT\t" << argR << endl;
				
				generateThatTraversal(root->child3,out);
				generateThatTraversal(root->child4,out);
			}else if(root->token1->tkType == DIVIDETk){
				argR = makeNameOrLabel("name");
				generateThatTraversal(root->child2,out);
				out << "\tSTORE\t" << argR << endl;
				generateThatTraversal(root->child1,out);
				out << "\tDIV\t" << argR << endl;
				
				generateThatTraversal(root->child3,out);
				generateThatTraversal(root->child4,out);
			}
		}else{
			generateThatTraversal(root->child1,out);
			
			generateThatTraversal(root->child2,out);
			generateThatTraversal(root->child3,out);
			generateThatTraversal(root->child4,out);
		}
	}else if(root->nonTerminalVal == "<T>"){
		//generate code for t and its children
		string argR;
		if(root->token1 != NULL){
			if(root->token1->tkType == PLUSTk){
				argR = makeNameOrLabel("name");
				generateThatTraversal(root->child2,out);
				out << "\tSTORE\t" << argR << endl;
				generateThatTraversal(root->child1,out);
				out << "\tADD\t" << argR << endl;
				
				generateThatTraversal(root->child3,out);
				generateThatTraversal(root->child4,out);
			}else if(root->token1->tkType == MINUSTk){
				argR = makeNameOrLabel("name");
				generateThatTraversal(root->child2,out);
				out << "\tSTORE\t" << argR << endl;
				generateThatTraversal(root->child1,out);
				out << "\tSUB\t" << argR << endl;
				
				generateThatTraversal(root->child3,out);
				generateThatTraversal(root->child4,out);
			}
		}else{
			generateThatTraversal(root->child1,out);
			
			generateThatTraversal(root->child2,out);
			generateThatTraversal(root->child3,out);
			generateThatTraversal(root->child4,out);
		}
	}else if(root->nonTerminalVal == "<F>"){
		//generate code for f and its children
		if(root->token1 != NULL){
			if(root->token1->tkType == MINUSTk){
				generateThatTraversal(root->child1,out);
				out << "\tMULT\t-1\n";
			}
			else{
				generateThatTraversal(root->child1,out);
			}
		}else{
			generateThatTraversal(root->child1,out);		
			generateThatTraversal(root->child2,out);
			generateThatTraversal(root->child3,out);
			generateThatTraversal(root->child4,out);
		}
	}else if(root->nonTerminalVal == "<R>"){
		//generate code for r and its children
		if(root->token1->tkType == IDTk){
			bool defined = verifyNode(root,false);
			if(defined){
				dist = find(root);
				if(dist >= 0){
					out << "\tSTACKR\t" << dist << endl;
				}
			}
		}else if(root->token1->tkType == NUMTk){
			out << "\tLOAD\t" << root->token1->value << endl;
		}else{
			generateThatTraversal(root->child1,out);
		}
		
		generateThatTraversal(root->child2,out);
		generateThatTraversal(root->child3,out);
		generateThatTraversal(root->child4,out);
	}else if(root->nonTerminalVal == "<stats>" || root->nonTerminalVal == "<mstats>" || root->nonTerminalVal == "<stat>"){
		//generate code for stats mstats and stat and their children
		generateThatTraversal(root->child1,out);
		generateThatTraversal(root->child2,out);
		generateThatTraversal(root->child3,out);
		generateThatTraversal(root->child4,out);
	}else if(root->nonTerminalVal == "<in>"){
		//generate code for in and its children
		bool defined = verifyNode(root,false);
		if(defined){
			dist = find(root);
			if(dist >= 0){
				out << "\tREAD\t" << root->token1->value << endl;
				out << "\tLOAD\t" << root->token1->value << endl;
				out << "\tSTACKW\t" << dist << endl;
			}
		}
	
		generateThatTraversal(root->child2,out);
		generateThatTraversal(root->child3,out);
		generateThatTraversal(root->child4,out);
	}else if(root->nonTerminalVal == "<out>"){
		//generate code for out and its children
		generateThatTraversal(root->child1,out);
		string argR = makeNameOrLabel("name");
		out << "\tSTORE\t" << argR << endl;
		out << "\tWRITE\t" << argR << endl;
		
		generateThatTraversal(root->child2,out);
		generateThatTraversal(root->child3,out);
		generateThatTraversal(root->child4,out);
	}else if(root->nonTerminalVal == "<if>"){
		//generate code for if and its children
		string label = makeNameOrLabel("label");
		string argR = makeNameOrLabel("name");
		generateThatTraversal(root->child3,out);
		out << "\tSTORE\t" << argR << endl;
		generateThatTraversal(root->child1,out);
		out << "\tSUB\t" << argR << endl;
		
		if(root->child2->token1->tkType == LTTk){
			out << "\tBRZPOS\t" << label << endl;
		}else if(root->child2->token1->tkType == EQLTk){
			out << "\tBRPOS\t" << label << endl;
		}else if(root->child2->token1->tkType == GTTk){
			out << "\tBRZNEG\t" << label << endl;
		}else if(root->child2->token1->tkType == EQGTk){
			out << "\tBRNEG\t" << label << endl;
		}else if(root->child2->token1->tkType == EQEQTk){
			out << "\tBRPOS\t" << label << endl;
			out << "\tBRNEG\t" << label << endl;
		}else if(root->child2->token1->tkType == EQNOTEQTk){
			out << "\tBRZERO\t" << label << endl;
		}
		
		generateThatTraversal(root->child4,out);
		out << endl << label << ":\tNOOP\n";
		
	}else if(root->nonTerminalVal == "<loop>"){
		//generate code for loop and its children
		string label = makeNameOrLabel("label");
		string label2 = makeNameOrLabel("label");
		string argR = makeNameOrLabel("name");
		
		out << endl << label << ":";
		
		generateThatTraversal(root->child3,out);
		out << "\tSTORE\t" << argR << endl;
		generateThatTraversal(root->child1,out);
		out << "\tSUB\t" << argR << endl;
		
		if(root->child2->token1->tkType == LTTk){
			out << "\tBRZPOS\t" << label2 << endl;
		}else if(root->child2->token1->tkType == EQLTk){
			out << "\tBRPOS\t" << label2 << endl;
		}else if(root->child2->token1->tkType == GTTk){
			out << "\tBRZNEG\t" << label2 << endl;
		}else if(root->child2->token1->tkType == EQGTk){
			out << "\tBRNEG\t" << label2 << endl;
		}else if(root->child2->token1->tkType == EQEQTk){
			out << "\tBRPOS\t" << label2 << endl;
			out << "\tBRNEG\t" << label2 << endl;
		}else if(root->child2->token1->tkType == EQNOTEQTk){
			out << "\tBRZERO\t" << label2 << endl;
		}
		
		generateThatTraversal(root->child4,out);
		out << "\tBR\t" << label << endl;
		out << endl << label2 << ":\tNOOP\n";
		
	}else if(root->nonTerminalVal == "<assign>"){
		//generate code for assign and its children
		generateThatTraversal(root->child1,out);
		bool defined = verifyNode(root,false);
		if(defined){
			dist = find(root);
			if(dist >= 0){
				out << "\tSTACKW\t" << dist << endl;
			}
		}
		
		generateThatTraversal(root->child2,out);
		generateThatTraversal(root->child3,out);
		generateThatTraversal(root->child4,out);
	}else if(root->nonTerminalVal == "<RO>"){
		//generate code for RO and its children
		generateThatTraversal(root->child1,out);
		generateThatTraversal(root->child2,out);
		generateThatTraversal(root->child3,out);
		generateThatTraversal(root->child4,out);
	}else{
		generateThatTraversal(root->child1,out);
		generateThatTraversal(root->child2,out);
		generateThatTraversal(root->child3,out);
		generateThatTraversal(root->child4,out);
	}
}   

//function to set variable or label name
string makeNameOrLabel(string nameOrLabel){
	string name;
	ostringstream convert;
	if(nameOrLabel == "name"){
		name = "V";
		convert << varCounter;
		name += convert.str();
		varCounter++;
	}else{
		name = "L";
		convert << labelCount;
		name += convert.str();
		labelCount++;
	}
	return name;
}

//function to find a variable on the stack
int find(tokenNode * root){
	bool found = false;
	int x;
	for(x = 0; x < stv->stv.size(); x++){
		if(root->token1->value == stv->stv[x]->token1->value){
			found = true;
			break;
		} 
	}
	if(!found){
		return -1;
	}else{
		return x;
	}
}
