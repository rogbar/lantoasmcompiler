/*
 * Roger Barker
 * CS4280 p3
 * mapper.h -- holds declarations for mapper()
 */

#ifndef MAPPER_H
#define MAPPER_H

#include <iomanip>
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include "token.h"
using namespace std;

int mapper(char testChar);

#endif
