/*
 * Roger Barker
 * CS4280 p3
 * scanner.cpp -- holds FADriver function, checkKW function and post-processor function
 */

#include "scanner.h"
#include "table.h"

//keywords to check
string keywords[15] = {
	"start","finish","then","if","repeat",
	"var","int","float","do","read","print",
	"void","return","dummy","program"
};

//static int line is the line number
static int line = 1;


//FADriver function returns a token to be put into the token vector
TokenType *scanner(FILE * fp){
	TokenType *tk = new TokenType;	//temp token variable

	int state = 0;		//current state
	int nextState = 0;	//next state
	int i = 0;			//iterator variable
	string tkString;	//temp string

	char testChar;		//character (taken from file)
	
	//find a token
	while (state < 100){
		testChar = getc(fp);	//get character from file
		if(testChar == '\n'){	//if character is \n increase line variable
			line++;
		}

		//handle comments
		if(testChar == '#'){
			do{
				testChar = getc(fp);
			}while(testChar != '\n');
			line++;	//have to increase line since we go to end of line
		}
		
		//handle case insensitivity
		if(isalpha(testChar)){
			testChar = tolower(testChar);
		}
		
		//convert character to string for appending in the future
		stringstream tmpstrstr;
		string tmpstr;
		tmpstrstr << testChar;
		tmpstrstr >> tmpstr;

		//get the next column to grab from the mapper function
		int nextCol = mapper(testChar);
		
		//error out when nextCol == 23
		//when this token returns the program will abort on invalid character
		if(nextCol == 23){
			tk->tkType = ERR;
			tk->value = "INV";
			tk->isKeyword = false;
			tk->line = line;
		}
		
		//Handle EOF
		if(testChar == EOF){
			tk->tkType = EOFTk;
			tk->value = "EOF";
			tk->isKeyword = false;
		}
		
		
		//get the next state from the table
		nextState = table[state][nextCol];
		
		//if the next state is IDTk generate a ID Token
		if(nextState == IDTk){
			tk->tkType = IDTk;
			tk->value = tkString;
			if(checkKW(tkString)){
				tk->isKeyword = true;
			}else{
				tk->isKeyword = false;
			}
		}
		
		//if next state is NUMTk generate a Number Token
		else if(nextState == NUMTk){
			tk->tkType = NUMTk;
			tk->value = tkString;
			tk->isKeyword = false;
		}
		
		//if next state is OPTk generate an Operator Token
		else if(nextState == OPTk){
			tk->tkType = OPTk;
			tk->value = tkString;
			tk->isKeyword = false;
		}
		
		//if next state is ROPTk generate a Relational Operator Token
		else if(nextState == ROPTk){
			tk->tkType = ROPTk;
			tk->value = tkString;
			tk->isKeyword = false;
		}
		
		//if next state is DELIMTk generate a Delimiter Token
		else if(nextState == DELIMTk){
			tk->tkType = DELIMTk;
			tk->value = tkString;
			tk->isKeyword = false;
		}
		
		//if nextState is ERR generate an Error Token
		else if(nextState == ERR){
			tk->tkType = ERR;
			tk->value = "ERROR";
			tk->isKeyword = false;
		}
		
		
		//if nextState is not a final state append the tmpstr created earlier to tkString
		tkString.append(tmpstr);
		
		i++;	//increment i
		state = nextState;	//set the current state
		
	}
	tk->line = line;	//set the line number for the token
	if((tk->tkType == IDTk || tk->tkType == NUMTk || 
		tk->tkType == OPTk || tk->tkType == ROPTk ||
		tk->tkType == DELIMTk)&& testChar != '\n'){
		ungetc(testChar,fp);	//unget c if necessary
	}
	
	tk = postProcessor(tk);
	return tk;	//return the token
}

//check if the IDTk is a keyword
bool checkKW(string tkStr){
	bool kw = false;
	for(int i = 0; i < 15; i++){
		if(tkStr == keywords[i]){
			kw = true;
		}
	}
	return kw;
}
