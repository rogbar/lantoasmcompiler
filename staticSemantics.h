/*
	Roger Barker
	CS4280 P3
	staticSemantics.h -- function and structure defs for staticSemantics
*/

#ifndef SEMANTICS_H
#define SEMANTICS_H

#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <vector>
#include "misc.h"
#include "token.h"
using namespace std;

typedef struct container STV;	//typedef for STV container

extern STV * stv;	//public external stv container

//container struct
struct container{
	vector <tokenNode*> stv; 
	int existingCount;
};

//variable struct
struct variable{
	string varName;
	int dist;
};

//function defs
void traverse(tokenNode * root, int depth);
void insertToSTV(tokenNode * root, int depth);
bool verifyNode(tokenNode * root,bool defineIt);
int find(tokenNode * root);
void generateThatTraversal(tokenNode * root,ofstream& out);
string makeNameOrLabel(string nameOrLabel);

#endif
