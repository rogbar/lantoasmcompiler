/*
 * Roger Barker
 * CS4280 p3
 * token.h -- holds enumerated tokenIDs and TokenType type definition.
 */

#ifndef TOKEN_H
#define TOKEN_H

#include <string>
#include <sstream>
#include <iomanip>
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <vector>
using namespace std;

/*
	Generic Token Types are valued between   100 and 149
	Keyword Token Types are valued between   150 and 199
	Delimiter Token Types are valued between 200 and 249
	Operator Token Types are valued between  250 and 299
	Relational Operator Token Types are valued between 300 and 349.
	
	Generic Tokens:
		IDTk    -- May be substituted with Specific keyword tokens
		NUMTk   -- Not substitutable
		DELIMTk -- Always substituted into a sub-token
		OPTk    -- Always substituted into a sub-token
		ROPTk   -- Always substituted into a sub-token
		EOFTk   -- Not substitutable
		ERR		-- Not substitutable
*/

typedef struct tkNd tokenNode;

typedef enum{
	IDTk 	    = 100,	NUMTk 	   = 101,	OPTk 	 	= 102, 
	ROPTk 	    = 103,	DELIMTk    = 104,	EOFTk 	 	= 105, 
	ERR		    = 106, 	STARTTk    = 150,	FINISHTk 	= 151,
	THENTk 	    = 152,	IFTk 	   = 153,	REPEATTk 	= 154,
	VARTk 	    = 155, 	INTTk 	   = 156,	FLOATTk	 	= 157,
	DOTk	    = 158,	READTk	   = 159,	PRINTTk	 	= 160,
	VOIDTk	    = 161,	RETURNTk   = 162,	DUMMYTk	 	= 163,
	PROGRAMTk   = 164,	PARENOTk   = 200,	PARENCTk 	= 201,
	SQBRACEOTk  = 202,	SQBRACECTk = 203,	CRLBRACEOTk = 204,
	CRLBRACECTk = 205,	SEMICTk    = 206,	DOTOPTk		= 207,
	COMMATk		= 208,	ASSTk	   = 250,   COLTk		= 251,
	PLUSTk		= 252,	MINUSTk	   = 253,	MULTTk		= 254,
	DIVIDETk	= 255,	MODTk	   = 256,	EQGTk		= 300,
	EQLTk		= 301,	EQEQTk	   = 302,	EQNOTEQTk	= 303,
	LTTk		= 304,	GTTk	   = 305
} tokenID;

typedef struct{
	string value;
	tokenID tkType;
	bool isKeyword;
	int line;
} TokenType;

struct tkNd{
	TokenType * token1;
	TokenType * token2;
	string nonTerminalVal;
	
	tkNd * parent;
	tkNd * child1;
	tkNd * child2;
	tkNd * child3;
	tkNd * child4;
};

#endif
