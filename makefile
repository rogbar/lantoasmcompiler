comp : main.o mapper.o misc.o parser.o scanner.o staticSemantics.o
	g++ -g -o comp main.o mapper.o misc.o parser.o scanner.o staticSemantics.o
	/bin/rm *.o

main.o : main.cpp
	g++ -c main.cpp

mapper.o : mapper.cpp
	g++ -c mapper.cpp

misc.o : misc.cpp
	g++ -c misc.cpp

parser.o : parser.cpp
	g++ -c parser.cpp

scanner.o : scanner.cpp
	g++ -c scanner.cpp

staticSemantics.o : staticSemantics.cpp
	g++ -c staticSemantics.cpp

rmo : 
	/bin/rm -f *.o

clean :
	/bin/rm -f comp *.asm

