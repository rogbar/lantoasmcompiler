/*
 * Roger Barker
 * CS 4280 -- Compilers
 * P3
 * misc.cpp includes function definitions for
 * tree traversals; space calculation; input (user or file)
 * and several mapping functions used throughout the project
 */
#include "misc.h"

//Verify input validates command arguments
int verifyFileInput(int argc, char** argv){
	cout << "verify file input\n";
	cout << "argc:\t"<<argc<<endl;
	string name;
	int ret;
	//process file arguments
	if(argc > 2){
		//too many arguments
		cout << "Error::Invalid number of arguments\n";
		cout << "Usage:: " << argv[0] << "[filename] [targetFileName]\nExit code: 5\n\n";
		ret = 5;
	}else if(argc == 2){
		cout << "got a file name ret = 1\n";
		ret = 1;
	}else{
		cout << "no file name user input\n";
		ret = 3;
	}
	cout << "ret: " << ret << endl;
	return ret;
}

//user input reads in from std in into a file out.lan which is used by the program
string userInput(){
		string inputStr;
		
		//get the outfile
		string outFile = "out.lan";
		
		//set up the ostream
		cout << "creating file: out.lan \n";
		ofstream out(outFile.c_str());
		
		//verify the file is opened
		if(!out.is_open()){
			cout << "failed to open file: " << outFile << " Exit 2\n";
			exit(2);
		} 
		//get input from command line push to out.dat
		cout << "Enter strings to insert into tree separate by whitespace. [ctrl+d] will end input phase\n";
		
		while(getline(cin,inputStr)){
			string buf;
			stringstream ss(inputStr);
			while(ss >> buf){
				out << buf + "\t";
			}
		}
		
		//close the file
		out.close();
		
		//exit if file failed to close for any reason
		if(out.is_open()){
			cout << "failed to close file: " << outFile << " Exit 2\n";
			exit(2);
		}
		
		//set filename = out.dat
		return outFile;
}

//postProcessor function does mapping of token subtypes and length validation
TokenType *postProcessor(TokenType *ttP){
	//assign values of token
	TokenType *tt = ttP;
	
	if(tt->tkType == IDTk || tt->tkType == NUMTk){
		if(tt->value.length() > 8){
			tt->value = tt->value.substr(0,8);
		}
	}		
	
	string val = tt->value;
	
	//substitute keyword tokens
	if(tt->tkType == IDTk && tt->isKeyword == true){
		//check for keywords
		if(val == "start")
			tt->tkType = STARTTk;
		else if(val == "finish")
			tt->tkType = FINISHTk;
		else if(val == "then")
			tt->tkType = THENTk;
		else if(val == "if")
			tt->tkType = IFTk;
		else if(val == "repeat")
			tt->tkType = REPEATTk;
		else if(val == "var")
			tt->tkType = VARTk;
		else if(val == "int")
			tt->tkType = INTTk;
		else if(val == "float")
			tt->tkType = FLOATTk;
		else if(val == "do")
			tt->tkType = DOTk;
		else if(val == "read")
			tt->tkType = READTk;
		else if(val == "print")
			tt->tkType = PRINTTk;
		else if(val == "void")
			tt->tkType = VOIDTk;
		else if(val == "return")
			tt->tkType = RETURNTk;
		else if(val == "dummy")
			tt->tkType = DUMMYTk;
		else if(val == "program")
			tt->tkType = PROGRAMTk;
	}
	
	//substitute tokens for Delimiter Tokens
	else if(tt->tkType == DELIMTk){
		if(val == "(")
			tt->tkType = PARENOTk;
		else if(val == ")")
			tt->tkType = PARENCTk;
		else if(val == "[")
			tt->tkType = SQBRACEOTk;
		else if(val == "]")
			tt->tkType = SQBRACECTk;
		else if(val == "{")
			tt->tkType = CRLBRACEOTk;
		else if(val == "}")
			tt->tkType = CRLBRACECTk;
		else if(val == ";")
			tt->tkType = SEMICTk;
		else if(val == ".")
			tt->tkType = DOTOPTk;
		else if(val == ",")
			tt->tkType = COMMATk;
	}
	
	//substitute tokens for Operator Tokens
	else if(tt->tkType == OPTk){
		if(val == "=")
			tt->tkType = ASSTk;
		else if(val == ":")
			tt->tkType = COLTk;
		else if(val == "+")
			tt->tkType = PLUSTk;
		else if(val == "-")
			tt->tkType = MINUSTk;
		else if(val == "*")
			tt->tkType = MULTTk;
		else if(val == "/")
			tt->tkType = DIVIDETk;
		else if(val == "%")
			tt->tkType = MODTk;
	}
	
	//substitute tokens for Relational Operator Tokens
	else if(tt->tkType == ROPTk){
		if(val == "=>")
			tt->tkType = EQGTk;
		else if(val == "=<")
			tt->tkType = EQLTk;
		else if(val == "==")
			tt->tkType = EQEQTk;
		else if(val == "=!=")
			tt->tkType = EQNOTEQTk;
		else if(val == "<")
			tt->tkType = LTTk;
		else if(val == ">")
			tt->tkType = GTTk;
	}
	
	return tt;
}

//printAppropro prints the appropriate token type on the screen
void printAppropro(tokenID ty){
	if(ty == IDTk)
		cout << "IDTk ";		//ID token type
	else if(ty == NUMTk)
		cout << "NUMTk ";		//Number token type
	else if(ty == OPTk)
		cout << "OPTk ";		//Operator token type
	else if(ty == ROPTk)
		cout << "ROPTk ";	  	//Relational operator token type
	else if(ty == DELIMTk)
		cout << "DELIMTk ";		//Delimeter token type
	else if(ty == ERR)
		cout << "ERR ";			//Error token type
	else if(ty == EOFTk)
		cout << "EOFTk ";		//End of file token type
	else if(ty == STARTTk)
		cout << "STARTTk ";		//Start token type
	else if(ty == FINISHTk)
		cout << "FINISHTk ";	//Finish token type
	else if(ty == THENTk)
		cout << "THENTk ";		//Then token type
	else if(ty == IFTk)
		cout << "IFTk ";		//If token type
	else if(ty == REPEATTk)
		cout << "REPEATTk ";	//Repeat token type
	else if(ty == VARTk)
		cout << "VARTk ";		//Var token type
	else if(ty == INTTk)
		cout << "INTTk ";		//Int token type
	else if(ty == FLOATTk)
		cout << "FLOATTk ";		//Float token type
	else if(ty == DOTk)
		cout << "DOTk ";		//Do token type
	else if(ty == READTk)
		cout << "READTk ";		//Read token type
	else if(ty == PRINTTk)
		cout << "PRINTTk ";		//Print token type
	else if(ty == VOIDTk)
		cout << "VOID ";		//Void token type
	else if(ty == RETURNTk)
		cout << "RETURNTk ";	//Return token type
	else if(ty == DUMMYTk)
		cout << "DUMMYTk ";		//Dummy token type
	else if(ty == PROGRAMTk)
		cout << "PROGRAMTk ";	//Program token type
	else if(ty == PARENOTk)
		cout << "PARENOTk ";	// ( token type
	else if(ty == PARENCTk)
		cout << "PARENCTk ";	// ) token type
	else if(ty == SQBRACEOTk)
		cout << "SQBRACEOTk ";	// [ token type
	else if(ty == SQBRACECTk)
		cout << "SQBRACECTk ";	// ] token type
	else if(ty == CRLBRACEOTk)
		cout << "CRLBRACEOTk ";	// { token type
	else if(ty == CRLBRACECTk)
		cout << "CRLBRACECTk ";	// } token type
	else if(ty == SEMICTk)
		cout << "SEMICTk ";		// ; token type
	else if(ty == DOTOPTk)
		cout << "DOTOPTk ";		// . token type
	else if(ty == COMMATk)
		cout << "COMMATk ";		// , token type
	else if(ty == ASSTk)
		cout << "ASSTk ";		// = token type
	else if(ty == COLTk)
		cout << "COLTk ";		// : token type
	else if(ty == PLUSTk)
		cout << "PLUSTk ";		// + token type
	else if(ty == MINUSTk)
		cout << "MINUSTk ";		// - token type
	else if(ty == MULTTk)
		cout << "MULTTk ";		// * token type
	else if(ty == DIVIDETk)
		cout << "DIVIDETk ";	// / token type
	else if(ty == MODTk)
		cout << "MODTk ";		// % token type
	else if(ty == EQGTk)
		cout << "EQGTk ";		// => token type
	else if(ty == EQLTk)
		cout << "EQLTk ";		// =< token type
	else if(ty == EQEQTk)
		cout << "EQEQTk ";		// == token type
	else if(ty == EQNOTEQTk)
		cout << "EQNOTEQTk ";	// =!= token type
	else if(ty == LTTk)
		cout << "LTTk ";		// < token type
	else if(ty == GTTk)
		cout << "GTTk ";		// > token type
}

//determine spacing to insert before the node & list of words
string spaces(int level){
    string space = "";

    //determine the amount of space to add befor printing
    //the node, should be 2 * the nodes level
    for(int i = 0; i < level*2; i++){
        space += " ";
    }
    return space;
}

int checkExtension(string targetFileName, string ext, bool input){
	cout << "check extension\n";
	//process filename
	int tmp = targetFileName.length();
	int ret;
		
	if(tmp > 4){
		cout << "tmp > 4\n";
		//if filename is more than 4 characters check if the extension is there.
		string arg = targetFileName.substr(tmp-4,tmp);
		if(input){
			cout << "input true\n";
			if(arg != ext && arg[0] == '.'){
				cout << "Error::Expected " << ext << " extension on input file.\nExit code: 7\n\n";
				ret = 7;
			}else if(arg != ext){
				cout << "true; ret = 2\n";
				ret = 2;
			}else{
				cout << "true; ret = 1\n";
				ret = 1;
			}
		}else if(arg != ext){
			cout << "false; ret = 2\n";
			ret = 2;
		}else{
			cout << "false; ret = 1\n";
			ret = 1;
		}
	}
	else{
		//if file is less than 4 characters need to append the extension
		cout << "tmp < 4; ret = 2\n";
		ret = 2;
	}
	return ret;
}

//function to error out
void error(string msg, int line){
	cout << msg << line << endl;
	exit(1);
}
