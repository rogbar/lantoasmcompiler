/*
 * Roger Barker
 * CS 4280 -- Compilers
 * P3 Parser and Tree Generator
 */

#include "parser.h"

//Parser function
tokenNode * parser(TokenType ** tt, FILE * fp){
	tokenNode * root = new tokenNode;
	root = programNT(tt,fp);
	if((*tt)->tkType == EOFTk){
		cout << "Parse Okay\n";
	}else{
		string msg = "Error::Expected EOF at line: ";
		error(msg,(*tt)->line);
	}
	return root;
}

//<program>  ->     <var> do <block>  return
tokenNode * programNT(TokenType ** tt, FILE * fp){
	string type = "<program>";
	tokenNode * crnt = new tokenNode;
	crnt = getNode(type);
	
	crnt->child1 = new tokenNode;
	crnt->child1 = varNT(tt,fp);
	assignParentAndDepth(&crnt,1);
	if((*tt)->tkType == DOTk){
		(*tt) = scanner(fp);
		crnt->child2 = new tokenNode;
		crnt->child2 = blockNT(tt,fp);
		assignParentAndDepth(&crnt,2);
		
		if((*tt)->tkType == RETURNTk){
			(*tt) = scanner(fp);
		}else{
			string msg = "Error::Expected 'return' at line: ";
			error(msg,(*tt)->line);
		}
	}else{
		string msg = "Error::Expected 'do' at line: ";
		error(msg, (*tt)->line);
	}
	return crnt;
}

//<block> -> start <var> <stats> finish
tokenNode * blockNT(TokenType ** tt, FILE* fp){
	string type = "<block>";
	tokenNode * crnt = new tokenNode;
	crnt = getNode(type);
	
	if((*tt)->tkType == STARTTk){
		(*tt) = scanner(fp);
		crnt->child1 = new tokenNode;
		crnt->child1 = varNT(tt,fp);
		assignParentAndDepth(&crnt,1);
		
		crnt->child2 = new tokenNode;
		crnt->child2 = statsNT(tt,fp);
		assignParentAndDepth(&crnt,2);
		
		if((*tt)->tkType == FINISHTk){
			(*tt) = scanner(fp);
		}else{
			string msg = "Error::Expected 'finish' at line: ";
			error(msg, (*tt)->line);
		}
	}else{
		string msg = "Error::Expected 'start' at line: ";
		error(msg, (*tt)->line);
	}
	return crnt;
}

//<var> -> empty | <type> ID <mvars> .
tokenNode * varNT(TokenType ** tt, FILE * fp){
	string type = "<var>";	
	tokenNode * crnt = new tokenNode;
	crnt = getNode(type);
	
	if((*tt)->tkType == VARTk){
		crnt->child1 = new tokenNode;
		crnt->child1 = typeNT(tt,fp);
		assignParentAndDepth(&crnt,1);
		if((*tt)->tkType == IDTk){
			crnt->token1 = (*tt);
			(*tt) = scanner(fp);
			crnt->child2 = new tokenNode;
			crnt->child2 = mvarsNT(tt,fp);
			assignParentAndDepth(&crnt,2);
			if((*tt)->tkType == DOTOPTk){
				(*tt) = scanner(fp);
			}else{
				string msg = "Error::Expected '.' at line: ";
				error(msg, (*tt)->line);
			}
		}else{
			string msg = "Error::Expected IDENTIFIER at line: ";
			error(msg, (*tt)->line);
		}
	}else{
		return NULL;
	}
	return crnt;
}

//<type>     ->     var
tokenNode * typeNT(TokenType ** tt, FILE * fp){
	string type = "<type>";
	tokenNode * crnt = new tokenNode;
	crnt = getNode(type);
	
	if((*tt)->tkType == VARTk){
		(*tt) = scanner(fp);
	}else{
		string msg = "Error::Expected 'var' at line: ";
		error(msg, (*tt)->line);
	}
	return crnt;
}

//<mvars> -> empty | : ID <mvars>
tokenNode * mvarsNT(TokenType ** tt, FILE * fp){
	string type = "<mvars>";
	tokenNode * crnt = new tokenNode;
	crnt = getNode(type);
	crnt->token1 = NULL;
	crnt->token2 = NULL;
	cout << endl;
	if((*tt)->tkType == COLTk){
		(*tt) = scanner(fp);
		if((*tt)->tkType == IDTk){
			crnt->token1 = (*tt);
			(*tt) = scanner(fp);
			
			crnt->child1 = new tokenNode;
			crnt->child1 = mvarsNT(tt,fp);
			
			assignParentAndDepth(&crnt,1);
			
			return crnt;
		}else{
			string msg = "Error::Expected IDENTIFIER at line: ";
			error(msg, (*tt)->line);
		}
	}else{
		return NULL;
	}
}

//<stats>    ->      <stat>  <mStat>
tokenNode * statsNT(TokenType ** tt, FILE * fp){
	string type = "<stats>";
	tokenNode * crnt = new tokenNode;
	crnt = getNode(type);
	
	crnt->child1 = new tokenNode;
	crnt->child1 = statNT(tt,fp);
	assignParentAndDepth(&crnt,1);
	
	crnt->child2 = new tokenNode;
	crnt->child2 = mStatNT(tt,fp);
	assignParentAndDepth(&crnt,2);
	return crnt;
}

//<mStat>    ->      empty | <stat>  <mStat>
tokenNode * mStatNT(TokenType ** tt, FILE * fp){
	string type = "<mStat>";
	tokenNode * crnt = new tokenNode;
	crnt = getNode(type);
	
	tokenID id = (*tt)->tkType;
	if(id != READTk && id != PRINTTk && id != STARTTk && id != IFTk && id != REPEATTk && id != IDTk){
		return NULL;
	}else{
		crnt->child1 = new tokenNode;
		crnt->child1 = statNT(tt,fp);
		assignParentAndDepth(&crnt,1);
		
		crnt->child2 = new tokenNode;
		crnt->child2 = mStatNT(tt,fp);
		assignParentAndDepth(&crnt,2);
	}
	return crnt;
}

//<stat>     ->      <in> | <out> | <block> | <if> | <loop> | <assign>
tokenNode * statNT(TokenType ** tt, FILE * fp){
	string type = "<stat>";
	tokenNode * crnt = new tokenNode;
	crnt = getNode(type);
	
	tokenID id = (*tt)->tkType;
	if(id == READTk){
		crnt->child1 = new tokenNode;
		crnt->child1 = inNT(tt,fp);
		assignParentAndDepth(&crnt,1);
	}else if(id == PRINTTk){
		crnt->child1 = new tokenNode;
		crnt->child1 = outNT(tt,fp);
		assignParentAndDepth(&crnt,1);
	}else if(id == STARTTk){
		crnt->child1 = new tokenNode;
		crnt->child1 = blockNT(tt,fp);
		assignParentAndDepth(&crnt,1);
	}else if(id == IFTk){
		crnt->child1 = new tokenNode;
		crnt->child1 = ifNT(tt,fp);
		assignParentAndDepth(&crnt,1);
	}else if(id == REPEATTk){
		crnt->child1 = new tokenNode;
		crnt->child1 = loopNT(tt,fp);
		assignParentAndDepth(&crnt,1);
	}else if(id == ASSTk || id == IDTk){
		crnt->child1 = new tokenNode;
		crnt->child1 = assignNT(tt,fp);
		assignParentAndDepth(&crnt,1);
	}else{
		string msg = "Error::Expected one of the following: read; print; start; if; repeat; = at line: ";
		error(msg, (*tt)->line);
	}
	
	return crnt;
}

//<in>       ->      read ID .
tokenNode * inNT(TokenType ** tt, FILE * fp){
	string type = "<in>";
	tokenNode * crnt = new tokenNode;
	crnt = getNode(type);
	
	if((*tt)->tkType == READTk){
		(*tt) = scanner(fp);
		if((*tt)->tkType == IDTk){
			crnt->token1 = (*tt);
			(*tt) = scanner(fp);
			if((*tt)->tkType == DOTOPTk){
				(*tt) = scanner(fp);
			}else{
				string msg = "Error::Expected '.' at line: ";
				error(msg, (*tt)->line);
			}
		}else{
			string msg = "Error::Expected IDENTIFIER at line: ";
			error(msg, (*tt)->line);
		}
	}else{
		string msg = "Error::Expected 'read' at line: ";
		error(msg, (*tt)->line);
	}
	
	return crnt;
}

//<out>      ->      print <expr>  .
tokenNode * outNT(TokenType ** tt, FILE * fp){
	string type = "<out>";
	tokenNode * crnt = new tokenNode;
	crnt = getNode(type);
		
	if((*tt)->tkType == PRINTTk){
		(*tt) = scanner(fp);
		crnt->child1 = new tokenNode;
		crnt->child1 = exprNT(tt,fp);
		assignParentAndDepth(&crnt,1);
		if((*tt)->tkType == DOTOPTk){
			(*tt) = scanner(fp);
		}else{
			string msg = "Error::Expected '.' at line: ";
			error(msg, (*tt)->line);
		}
	}else{
		string msg = "Error::Expected 'print' at line: ";
		error(msg, (*tt)->line);
	}
	return crnt;
}

//<if>       ->      if [ <expr> <RO> <expr> ]  <block>        
tokenNode * ifNT(TokenType ** tt, FILE * fp){
	string type = "<if>";
	tokenNode * crnt = new tokenNode;
	crnt = getNode(type);
	
	if((*tt)->tkType == IFTk){
		(*tt) = scanner(fp);
		if((*tt)->tkType = SQBRACEOTk){
			(*tt) = scanner(fp);
			crnt->child1 = new tokenNode;
			crnt->child1 = exprNT(tt,fp);
			assignParentAndDepth(&crnt,1);
			
			crnt->child2 = new tokenNode;
			crnt->child2 = RONT(tt,fp);
			assignParentAndDepth(&crnt,2);
			
			crnt->child3 = new tokenNode;
			crnt->child3 = exprNT(tt,fp);
			assignParentAndDepth(&crnt,3);
			
			if((*tt)->tkType == SQBRACECTk){			
				(*tt) = scanner(fp);
				crnt->child4 = new tokenNode;
				crnt->child4 = blockNT(tt,fp);
				assignParentAndDepth(&crnt,4);
				
			}else{
				string msg = "Error::Expected ']' at line: ";
				error(msg, (*tt)->line);
			}
		}else{
			string msg = "Error::Expected '[' at line: ";
			error(msg, (*tt)->line);
		}
	}else{
		string msg = "Error::Expected 'if' at line: ";
		error(msg, (*tt)->line);
	}
	return crnt;
}
     
//<loop>     ->      repeat [ <expr> <RO> <expr> ] <block>
tokenNode * loopNT(TokenType ** tt, FILE * fp){
	string type = "<loop>";
	tokenNode * crnt = new tokenNode;
	crnt = getNode(type);
	
	if((*tt)->tkType == REPEATTk){
		(*tt) = scanner(fp);
		if((*tt)->tkType == SQBRACEOTk){
			(*tt) = scanner(fp);
			crnt->child1 = new tokenNode;
			crnt->child1 = exprNT(tt,fp);
			assignParentAndDepth(&crnt,1);
			
			crnt->child2 = new tokenNode;
			crnt->child2 = RONT(tt,fp);
			assignParentAndDepth(&crnt,2);
			
			crnt->child3 = new tokenNode;
			crnt->child3 = exprNT(tt,fp);
			assignParentAndDepth(&crnt,3);
			
			if((*tt)->tkType == SQBRACECTk){
				(*tt) = scanner(fp);
				crnt->child4 = new tokenNode;
				crnt->child4 = blockNT(tt,fp);
				assignParentAndDepth(&crnt,4);
				
			}else{
				string msg = "Error::Expected ']' at line: ";
				error(msg, (*tt)->line);
			}
		}else{
			string msg = "Error::Expected '[' at line: ";
			error(msg, (*tt)->line);
		}
	}else{
		string msg = "Error::Expected 'repeat' at line: ";
		error(msg, (*tt)->line);
	}
	return crnt;
}

//<assign>   ->      ID = <expr> .
tokenNode * assignNT(TokenType ** tt, FILE * fp){
	string type = "<assign>";
	tokenNode * crnt = new tokenNode;
	crnt = getNode(type);

	if((*tt)->tkType == IDTk){
		crnt->token1 = (*tt);
		(*tt) = scanner(fp);
		if((*tt)->tkType == ASSTk){
			crnt->token2 = (*tt);
			(*tt) = scanner(fp);
			crnt->child1 = new tokenNode;
			crnt->child1 = exprNT(tt,fp);
			assignParentAndDepth(&crnt,1);
			
			if((*tt)->tkType == DOTOPTk){
				(*tt) = scanner(fp);
			}else{
				string msg = "Error::Expected '.' at line: ";
				error(msg, (*tt)->line);
			}
		}else{
			string msg = "Error::Expected '=' at line: ";
			error(msg, (*tt)->line);
		}
	}else{
		string msg = "Error::Expected IDENTIFIER at line: ";
		error(msg, (*tt)->line);
	}
	return crnt;
}

//<expr>     ->      <T> * <expr> | <T> / <expr> | <T>
tokenNode * exprNT(TokenType ** tt, FILE * fp){
	string type = "<expr>";
	tokenNode * crnt = new tokenNode;
	crnt = getNode(type);
	
	crnt->child1 = new tokenNode;
	crnt->child1 = TNT(tt,fp);
	assignParentAndDepth(&crnt,1);
	
	if((*tt)->tkType == MULTTk || (*tt)->tkType == DIVIDETk){
		crnt->token1 = (*tt);
		(*tt) = scanner(fp);
		crnt->child2 = new tokenNode;
		crnt->child2 = exprNT(tt,fp);
		assignParentAndDepth(&crnt,2);
	}
	return crnt;
}

//<T>        ->      <F> + <T> | <F> - <T> | <F>
tokenNode * TNT(TokenType ** tt, FILE * fp){
	string type = "<T>";
	tokenNode * crnt = new tokenNode;
	crnt = getNode(type);

	crnt->child1 = new tokenNode;
	crnt->child1 = FNT(tt,fp);
	assignParentAndDepth(&crnt,1);
	if((*tt)->tkType == PLUSTk || (*tt)->tkType == MINUSTk){
		crnt->token1 = (*tt);
		(*tt) = scanner(fp);
		crnt->child2 = new tokenNode;
		crnt->child2 = TNT(tt,fp);
		assignParentAndDepth(&crnt,2);
	}
	return crnt;
}

//<F>        ->      - <F> | <R>
tokenNode * FNT(TokenType ** tt, FILE * fp){
	string type = "<F>";
	tokenNode * crnt = new tokenNode;
	crnt = getNode(type);
	

	if((*tt)->tkType == MINUSTk){
		crnt->token1 = (*tt);
		(*tt) = scanner(fp);
		crnt->child1 = new tokenNode;
		crnt->child1 = FNT(tt,fp);
		assignParentAndDepth(&crnt,1);
	}else{
		crnt->child1 = new tokenNode;
		crnt->child1 = RNT(tt,fp);
		assignParentAndDepth(&crnt,1);
	}
	
	return crnt;
}

//<R>        ->      (<expr>) | ID | Number 
tokenNode * RNT(TokenType ** tt, FILE * fp){
	string type = "<R>";
	tokenNode * crnt = new tokenNode;
	crnt = getNode(type);
	
	if((*tt)->tkType == IDTk || (*tt)->tkType == NUMTk){
		crnt->token1 = (*tt);
		(*tt) = scanner(fp);
	}else if((*tt)->tkType == PARENOTk){
		(*tt) = scanner(fp);
		crnt->token1 = (*tt);
		crnt->child1 = new tokenNode;
		crnt->child1 = exprNT(tt,fp);
		assignParentAndDepth(&crnt,1);
		if((*tt)->tkType == PARENCTk){
			crnt->token2 = (*tt);
			(*tt) = scanner(fp);
		}else{
			string msg = "Error::Expected ')' at line: ";
			error(msg, (*tt)->line);
		}
	}else{
		string msg = "Error::Expected one of the following: '('; IDENTIFIER; NUMBER at line: ";
		error(msg, (*tt)->line);
	}
	return crnt;
}

//<RO>       ->      =< | => | == |  > | <  |  =!=
tokenNode * RONT(TokenType ** tt, FILE * fp){
	string type = "<RO>";
	tokenNode * crnt = new tokenNode;
	crnt = getNode(type);
	
	tokenID id = (*tt)->tkType;
	if(id != EQLTk && id != EQGTk && id != EQEQTk && id != GTTk && id != LTTk && id != EQNOTEQTk){
		string msg = "Error:: Expected one of the following: '=<'; '=>'; '=='; '>'; '<'; '=!=' at line: ";
		error(msg, (*tt)->line);
	}else{
		crnt->token1 = (*tt);
		(*tt) = scanner(fp);
	}
	
	return crnt;
}

tokenNode * getNode( string typeOfNT ){
	tokenNode * node = new tokenNode;
	node->nonTerminalVal = typeOfNT;
	node->token1 = NULL;
	node->token2 = NULL;
	node->child1 = NULL;
	node->child2 = NULL;
	node->child3 = NULL;
	node->child4 = NULL;
	return node;
}

void assignParentAndDepth( tokenNode ** par, int num){
	if(num == 1 && (*par)->child1 != NULL){
		(*par)->child1->parent = (*par);
	}else if(num == 2 && (*par)->child2 != NULL){
		(*par)->child2->parent = (*par);
	}else if(num == 3 && (*par)->child3 != NULL){
		(*par)->child3->parent = (*par);
	}else if(num == 4 && (*par)->child4 != NULL){
		(*par)->child4->parent = (*par);
	}
}
