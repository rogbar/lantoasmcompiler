/*
 * Roger Barker
 * CS 4280 - Compilers
 * P3 -- main.cpp is the main driver for p2
 */
#include "misc.h"
#include "parser.h"
#include "staticSemantics.h"

int main(int argc, char ** argv){

	//set up variables as necessary
	FILE * fp; 					//the file to grab tokens from
	int verify = 0;
	bool ui = false;
	bool targetNamePassed = false;
	string name;
	string targetFileName;
	TokenType *tmpTT;			//tmp TokenType instance to hold result of FADriver
	vector<TokenType*> ttvec;	//vector of Tokens
	
	if(argc == 3){
		cout << "3 args\n";
		targetFileName = argv[2];
		cout << "targetFileName: " << targetFileName << endl;
		verify = checkExtension(targetFileName,".asm",false);
		if(verify == 7 || (verify != 1 && verify != 2)){
			cout << "Error::Invalid input exit 7\n\n";
			return 7;
		}else if(verify == 2){
			//append extension
			cout << "append file name:\t";
			targetFileName.append(".asm");
			cout << targetFileName << endl;
		}
		targetNamePassed = true;
		argc = 2;	//we processed the argument.
	}
	cout << "argc: " << argc << endl;
	//verify arguments to file
	verify = verifyFileInput(argc,argv);
	if(verify == 5 || (verify != 1 && verify != 3)){
		cout << "invalid args\n";
		return 5;
	}else if(verify == 1){
		cout << "get ready to verify\n";
		name = argv[1];
		cout << "name: " << name << endl;
		verify = checkExtension(name,".lan",true);
		if(verify == 7 || (verify != 1 && verify != 2)){
			cout << "Error::Invalid input exit 7\n\n";
			return 7;
		}else if(verify == 2){
			cout << "verify is 2\n";
			//append extension
			name.append(".lan");
		}
	}else if(verify == 3){
		//we are requesting user input
		cout << "ui\n";
		ui = true;
		//get the user input
		name = userInput();
	}
	
	//assign target File name to be lan.out
	if(ui && !targetNamePassed){
		targetFileName = "lan.out";
	}else if(!targetNamePassed){
		//set the target file name
		string tmpName = name;
		int len = tmpName.length();
		tmpName = tmpName.substr(0,len-4);
		//assign .asm extension
		tmpName.append(".asm");
		targetFileName = tmpName;
	}
	
	//open the target file for writing
	ofstream asmOut (targetFileName.c_str());
	if(asmOut.is_open()){
		cout << "opened the file\n";
	}else{
		cout << "no file\n";
		//error if the outfile doesn't open
		error("Failed to open File",0);
	}
	
	//open the file for reading
    fp = fopen(name.c_str(),"r");
	if(!fp){
		error("ERROR::No such file",-1);
	}
	tmpTT = scanner(fp);	//get a token from the file
	
	if(tmpTT->value == "EOF"){
		//if token is EOF stop processing file
		ttvec.push_back(tmpTT);	//add the token to the token vector
	}
	else if(tmpTT->value == "INV"){
		//if invalid character is found exit program with code 10 for fail
		cout << "ERROR::Invalid Character found at line: " << tmpTT->line << endl;
		return 10;
	}
	tokenNode * root;
	root = parser(&tmpTT,fp);	//parse the file & build the tree
	traverse(root,0);	//print the tree
	generateThatTraversal(root, asmOut);	//generate code
	
	
	//if no errors move to code generation
	
	//close the source file
	fclose(fp);
	asmOut.close();
	
	//if input was user input remove out.dat
	if(ui){
		cout << "removing out.dat \n";
		if(remove("out.lan")!=0){
			cout << "failed to delete file: out.dat Exit 9\n";
			return 9;
		}else{
			cout << "out.dat deleted\n";
		}
	}
	
	return 0;
}
