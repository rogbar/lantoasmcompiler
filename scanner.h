/*
 * Roger Barker
 * CS4280 p3
 * scanner.h -- holds definitions for FADriver function, checkKW function and post-processor function
 */

#ifndef SCANNER_H
#define SCANNER_H

#include <string>
#include <sstream>
#include <iomanip>
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <fstream>
#include <vector>
#include "token.h"
#include "mapper.h"
#include "misc.h"
using namespace std;

typedef struct{
	string chars;
	int lineNo;
} ScannedChar;


TokenType *scanner(FILE *fp);
bool checkKW(string tkStr);


#endif
